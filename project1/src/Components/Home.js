import React, { useState } from 'react';
import { Grid, Container, Typography } from "@mui/material";
import './Home.css';

const Home = () => {
    return (
        <Container style={{ maxWidth: '100%', maxHeight: '100%', display: 'flex', paddingTop: '3%', paddingLeft: "3%", paddingRight: "3%" }}>
            <Grid item xl={12} lg={12} md={12} sm={12} xs={12} style={{ width: '100%', display: 'flex', justifyContent: 'space-around' }}>
                <Grid item xs={4} style={{ display:'flex', justifyContent:'center', alignItems:'center' }}>
                    <Typography variant='h2' component='h2' style={{position:'absolute', left:40, top:160}}>
                        Sahariiiii
                    </Typography>
                    <Typography variant='p' component='p' style={{color:'black', position:'absolute', left:95, fontSize:30, fontWeight:'bold', top:270}}>
                        Home
                    </Typography>
                    <Typography variant='p' component='p' style={{color:'black', position:'absolute', left:95, fontSize:30, fontWeight:'bold', top:330}}>
                        Biography
                    </Typography>
                    <Typography variant='p' component='p' style={{color:'black', position:'absolute', left:95, fontSize:30, fontWeight:'bold', top:380}}>
                        Contact
                    </Typography>
                    <div className='line-5'>

                    </div>
                </Grid>
                <Grid item xl={8} lg={6} sm={12} xs={12}>
                    <Typography variant='img' component='img' src='/public/Assets/media/back.jpg' style={{ width:100, height:100}}>

                    </Typography>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Home;